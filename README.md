## Generate ssh key on host
``` bash
ssh-keygen -t rsa

```

## Copy public key to other machines
``` bash
ssh-copy-id user@remote_host
```


# Run ansible playbook(provide become passwd)
``` bash
cd ansible && ansible-playbook -K -i hosts.ini playbook.yml 

```